# Survey on Housework Routines

This page contains the additional material of a series of surveys that we will use to capture qualitative data about housework routines.

# Organization

Each survey materials will be stored in a folder named with the pattern `survey-x`, where `x` corresponds to the survey order.

The `analyses` folder contains all the analyses of pilot studies (before the preregistration) and the analysis files of the main study (after the preregistration).

The `html` folder contains the source-code of the survey webpage.

The `results` folder contains the participant responses.

The `extra` folder contains additional material such as the Prolific task description.