<div id="information-alert" class="alert alert-primary alert-dismissible" role="alert">
  <?php echo $alert_text ?>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
