function getBaselineMessage(numberOfWords, wordLimit) {
    return numberOfWords >= wordLimit ? "Write more to increase the bonus" : "";
}

function getBonusMessage(numberOfWords, wordLimit) {
    var msg = "";
    msg = msg + numberOfWords > wordLimit ? `${numberOfWords - wordLimit} extra words` : "";
    if (numberOfWords > wordLimit + 500) {
        msg = msg + ". That's fantastic.";
    } else if (numberOfWords > wordLimit + 250) {
        msg = msg + ". That's great.";
    } else if (numberOfWords > wordLimit + 100) {
        msg = msg + ". That's good.";
    } else if (numberOfWords > wordLimit + 50) {
        msg = msg + ". That's fair.";
    }
    return msg;
}

function countWords(textAreaId, defaultBarId, extraBarId, wordCounterId, wordLimit = 200) { 
    // Get the input text value 
    var text = $(textAreaId).val(); 
    // Split the words on each space character  
    var words = text.split(/[\s!?:.,;]+/); 

    // Count how many words exist in the array
    var numberOfWords = 0;
    words.forEach(word => {
        if (word !== "") {
            numberOfWords++;
        }
    });

    // Update the progress bars
    $(defaultBarId)
        .attr('aria-valuenow', numberOfWords)
        .css('width', `${numberOfWords <= wordLimit ? numberOfWords / wordLimit * 100 : 100}%`)
        .html(getBaselineMessage(numberOfWords, wordLimit));
    $(extraBarId)
        .attr('aria-valuenow', numberOfWords)
        .css('width', `${numberOfWords > wordLimit ? (numberOfWords - wordLimit) / wordLimit * 100 : 0}%`)
        .html(getBonusMessage(numberOfWords, wordLimit));

    // Display it as output 
    $(wordCounterId).html(numberOfWords === 1 ? `${numberOfWords} word` : `${numberOfWords} words`);

    return numberOfWords;
}