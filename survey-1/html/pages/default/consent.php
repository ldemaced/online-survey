<div class="row">
    <div class="col">
      <h2>Detailed information notice and informed consent</h2>
      <div class="consent">
        <p><strong>Baseline reward</strong>. You will earn £<?php echo sprintf('%0.2f', $config["Prolific"]["baseline_payment"]); ?> if you answer all required questions and meet the minimum number of 200 words required in the first question. All answers will be examined and, if no dishonest practice is identified (for example, writing or copying nonsensical, irrelevant, or clearly plagiarized text), you will be paid.</p>
        <p><strong>Bonus</strong>. You will earn a bonus of up to £<?php echo sprintf('%0.2f', $config["Prolific"]["bonus_payment"]); ?> if you provide useful answers. After the study, two researchers will rate all participant responses according to the length and quality of their answers. Participants will earn a bonus proportional to their rating.</p>
        <p><strong>Withdraw</strong>. Your participation is voluntary, and you may stop at any time without providing a reason and without penalty. However, we can only pay you if you complete the entire survey.</p>
        <p><strong>Risks</strong>. Your participation in this study does not present any foreseeable risks beyond those experienced in daily life.</p>
        <p><strong>Privacy</strong>. Your participation is confidential, and all responses will be stored anonymously on a private server. To ensure anonymity, we ask you not to provide any identifying information such as people's names or emails. We will check for the presence of identifiable information and will remove it.</p>
        <p><strong>Data access</strong>. The data will be made publicly available on an open science platform upon the completion of the research.</p>
        <p><strong>Contact</strong>. This survey has been approved by the <?php echo $config["Prolific"]["CER"]; ?>. If you have any questions, suggestions, or complaints, please contact Pierre Dragicevic (pierre.dragicevic@inria.fr), the main researcher.</p>
        <form>
            <input type="checkbox" id="agrees-information" name="agrees-information">
            <label for="agrees-information">I agree with the study terms</label>
        </form>
      </div>
    </div>
</div>

<script>
// make button active as soon as value was changed
$('#agrees-information').on('change', function() {
    $("#btn_<?php echo $id;?>").prop('disabled', $(this).is(":not(:checked)"));
});
</script>