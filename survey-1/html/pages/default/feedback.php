<div class="row">
  <div class="col">
    <h2>Do you have any feedback about this study?</h2>
    <?php
      $alert_text='This question is optional but can count towards the bonus';
      include 'html/components/information.php';
    ?>
    <form class="mt-2 form-group" action="" method="post">
      <textarea class="form-control" id="optionalComments" rows="8" cols="80"></textarea>
      <h5>Are you interested in participating in follow-up studies?</h5>
      <p class="text-secondary">If you agree, we can add you to a whitelist. We might contact you for future surveys or interviews.</p>
      <input type="checkbox" id="wants-participate" name="wants-participate">
      <label for="wants-participate">I accept to participate in further studies</label>
    </form>
  </div>
</div>


<script type="text/javascript">

  // This is the event triggered to save the data entered. The event triggers when the 'next' button is pressed.
	$('body').on('next', function(e, type){
		// The if clause below ensures that this specific instance of a next button press is only triggered when the id of the element corresponds to the one being defined above.
    if (type === '<?php echo $id;?>' && !(typeof measurements === 'undefined')){
      measurements['optionalComments'] = $("#optionalComments").val().replace(/"/g, "'");
      measurements['furtherExperiments'] = $("#wants-participate").is(":checked") ? "Yes" : "No";
		}
	});
</script>
