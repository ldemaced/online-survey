<div class="row">
  <div class="col">
    <h2>Survey on housework routines</h2>
    <p>This survey will help us understand how people organize their housework routines.</p>
    <p>You will be asked to give detailed information (3 questions, about 200 words each) about your household's housework routines. You can write casually but avoid using acronyms or slangs.</p>
    <p>Here is a 200-word text to give you a rough idea:</p>
    <blockquote class="mx-4 bg-light">
    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed felis nisi, tempus sit amet ligula eu, egestas interdum quam. Quisque dignissim purus erat, vel pretium lectus molestie in. Duis vitae dui mollis, feugiat purus vel, pretium nisl. Aenean cursus mauris eget dui iaculis, nec iaculis odio congue.  Aenean placerat, urna faucibus nisi, et accumsan ligula metus a nisi. In hac habitasse platea dictumst. Quisque mollis sollicitudin ligula, eu mattis ipsum cursus vitae. Nunc quis ipsum id metus mattis luctus non quis mauris. Nam ut euismod mi. Maecenas faucibus nunc massa, eu commodo eros lobortis nec. Ut nec leo non orci lobortis accumsan. Nulla tempus varius justo vitae suscipit. Pellentesque tempor nunc erat, et sodales sem scelerisque ac.
Curabitur accumsan, massa quis tincidunt aliquet, orci orci egestas quam, eu sagittis eros sem eu tellus. Nam eget mi id sem pharetra convallis quis nec mi. Maecenas sodales interdum arcu, a eleifend lorem placerat vitae. Ut rutrum, nibh id euismod pulvinar, enim mi gravida ligula, nec venenatis turpis massa in lectus. Mauris id interdum nisl, vitae feugiat tortor. Aenean efficitur at augue nec pellentesque.  Suspendisse elementum eros nec urna sollicitudin, id sagittis enim interdum. Aenean rhoncus vestibulum velit, vel aliquam leo condimentum vel.
    </blockquote>
    <p>The duration of the study (about <?php echo sprintf('%d', $config["Prolific"]["experiment_min_duration"]); ?> to <?php echo sprintf('%d', $config["Prolific"]["experiment_max_duration"]); ?> minutes) and your payment (£<?php echo sprintf('%0.2f', $config["Prolific"]["baseline_payment"]); ?> to £<?php echo sprintf('%0.2f', $config["Prolific"]["baseline_payment"] + $config["Prolific"]["bonus_payment"]); ?>) will depend on how detailed your answers are.</p>
  </div>
</div>
