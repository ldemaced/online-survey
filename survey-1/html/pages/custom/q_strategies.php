<div class="row">
    <div class="col">
        <h2>Describe the strategies your household uses to coordinate the housework</h2>
        <p class="text-secondary">Explain the rules or approaches that your household use to identify, negotiate, and keep track of the housework. Questions that might help: How do people know what needs to be done? How do people negotiate who does what? How do people know who did what? Tell us about the strategies that work poorly, and those that work really well, and why.</p>
        <?php
        $alert_text='We recommend you write more than 200 words to get a high bonus';
        include 'html/components/information.php';
        ?>
        <div class="form-group">
            <textarea class="form-control" name="qa_strategies" id="qa_strategies" oninput="countWords('#qa_strategies', '#dpb_strategies', '#spb_strategies', '#wc_strategies')" cols="135" rows="15"></textarea>
            <div class="progress">
                <div id="dpb_strategies" class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="200"></div>
                <div id="spb_strategies" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="400"></div>
            </div>
            <p class="text-small" id="wc_strategies">0 words</p>
        </div>
    </div>
</div>

<script>
$('body').on('next', function(e, type){
    if (type === '<?php echo $id;?>' && !(typeof measurements === 'undefined')){
      measurements['strategies'] = $("#qa_strategies").val().replace(/"/g, "'");
	}
});

    // function countWords(textAreaId, defaultBarId, extraBarId, wordCounterId, wordLimit = 400) {
    //     var intermediateLimit = wordLimit / 2;

    //     // Get the input text value 
    //     var text = $(textAreaId).val(); 
    //     // Split the words on each space character  
    //     var words = text.split(' '); 

    //     // Count how many words exist in the array
    //     var numberOfWords = 0;
    //     words.forEach(word => {
    //         if (word !== "") {
    //             numberOfWords++;
    //         }
    //     });

    //     // Update the progress bars
    //     $(defaultBarId).attr('aria-valuenow', numberOfWords).css('width', `${numberOfWords <= intermediateLimit ? numberOfWords / wordLimit * 100 : 50}%`);
    //     $(extraBarId)
    //         .attr('aria-valuenow', numberOfWords)
    //         .css('width', `${numberOfWords > intermediateLimit ? (numberOfWords - intermediateLimit) / wordLimit * 100 : 0}%`)
    //         .html(numberOfWords > intermediateLimit ? `${numberOfWords - intermediateLimit} extra words` : "");

    //     // Display it as output 
    //     $(wordCounterId).html(numberOfWords === 1 ? `${numberOfWords} word` : `${numberOfWords} words`);
    // }
</script>