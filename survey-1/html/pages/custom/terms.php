<div class="row">
    <div class="col">
        <h2>Clarifying terms</h2>
        <p>This is what we understand for:</p>
        <p><strong>Household</strong> = People who share the same house, either family or nonfamily (for example, roommates).</p>
        <p><strong>Housework</strong> = Unremunerated work of maintaining a household that is performed by household members. For example, cleaning, tidying, cooking, childcare, walking the dog, etc.</p>
    </div>
</div>