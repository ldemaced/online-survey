<div class="row">
    <div class="col">
        <h2>Recount situations where household members disagreed because of housework, and express your thoughts and feelings about it</h2>
        <p class="text-secondary">You can focus on a single event or report several events. Try to provide as much detail as possible. Questions that might help: When, where, and why did the conflict happen? Who was involved? How did/do you feel about that situation? Do similar conflicts still happen? Why? How could this type of situation be improved in the future?</p>
        <?php
        $alert_text='We recommend you write more than 200 words to get a high bonus';
        include 'html/components/information.php';
        ?>
        <div class="form-group">
            <textarea class="form-control" name="qa_conflicts" id="qa_conflicts" oninput="countWords('#qa_conflicts', '#dpb_conflicts', '#spb_conflicts', '#wc_conflicts')" cols="135" rows="15"></textarea>
            <div class="progress">
                <div id="dpb_conflicts" class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="200"></div>
                <div id="spb_conflicts" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="400"></div>
            </div>
            <p class="text-small" id="wc_conflicts">0 words</p>
        </div>
    </div>
</div>

<script>
$('body').on('next', function(e, type){
    if (type === '<?php echo $id;?>' && !(typeof measurements === 'undefined')){
      measurements['conflicts'] = $("#qa_conflicts").val().replace(/"/g, "'");
	}
});
</script>