<div class="row">
    <div class="col">
        <h2>Explain how the housework is distributed within your current household, and what you think and feel about that division of tasks</h2>
        <p class="text-secondary">Mention people who contribute to housework (no personal information; examples: "my wife", "my roommate", or "my older brother" are OK), explain who is in charge of which task, and express your thoughts and feelings about what you like and dislike in the way the work is divided.</p>
        <?php
        $alert_text='<strong>Required</strong>. You need to write at least 200 words to continue and receive the baseline payment. We recommend you write more to get a bonus.';
        include 'html/components/information.php';
        ?>
        <div class="form-group">
            <textarea class="form-control" name="qa_household_profile" id="qa_household_profile" oninput="countWordsAndEnable('#qa_household_profile', '#dpb_household_profile', '#spb_household_profile', '#wc_household_profile')" cols="135" rows="15"></textarea>
            <div class="progress">
                <div id="dpb_household_profile" class="progress-bar" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="200"></div>
                <div id="spb_household_profile" class="progress-bar bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="400"></div>
            </div>
            <p class="text-small" id="wc_household_profile">0 words</p>
        </div>
    </div>
</div>

<script>
function countWordsAndEnable(textAreaId, defaultBarId, extraBarId, wordCounterId) { 
    var wordLimit = 200;
    var numberOfWords = countWords(textAreaId, defaultBarId, extraBarId, wordCounterId, wordLimit);

    // Unlock next question
    if (numberOfWords >= wordLimit) {
        $("#btn_<?php echo $id;?>").prop('disabled', false);
    } else {
        $("#btn_<?php echo $id;?>").prop('disabled', true);
    }
}

$('body').on('next', function(e, type){
    if (type === '<?php echo $id;?>' && !(typeof measurements === 'undefined')){
      measurements['householdProfile'] = $("#qa_household_profile").val().replace(/"/g, "'");
	}
});
</script>