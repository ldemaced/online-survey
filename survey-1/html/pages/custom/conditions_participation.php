<div class="row">
    <div class="col">
        <h2>Conditions for participation</h2>
        <p>Please make sure you satisfy the following conditions before proceeding with this study:</p>
        <ol>
            <li>I am 18 years or older</li>
            <li>I am fluent in English (reading and writing)</li>
            <li>My current household comprises at least two persons who share the housework</li>
            <li>I am relatively familiar with the housework routines in my house</li>
        </ol>
        <form>
            <input type="checkbox" id="meets-conditions" name="meets-conditions">
            <label for="meets-conditions">I meet all of the conditions</label>
        </form>
    </div>
</div>

<script>
// make button active as soon as value was changed
$('#meets-conditions').on('change', function() {
    $("#btn_<?php echo $id;?>").prop('disabled', $(this).is(":not(:checked)"));
});
</script>